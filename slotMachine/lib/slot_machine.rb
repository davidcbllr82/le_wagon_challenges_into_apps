class SlotMachine
  def initialize(amount)
    @amount = amount
  end
  SCORE = {
    'cherry' => 10,
    'seven' => 20,
    'bell' => 30,
    'stars' => 40,
    'joker' => 50
  }
  def score(reels)
    # reels = ['stars', 'stars', 'stars']
    if reels.uniq.length == 1
      # CALCULATE THE SCORE FOR 3 identical SYMBOLS
      reel = reels.last
      return SCORE[reel]
    elsif reels.count('joker') == 2
      return SCORE['joker'] / 2
    elsif reels.uniq.length == 2 && reels.include?('joker')
      reels.delete('joker')
      reel = reels.last
      return SCORE[reel] / 2
    else
      return 0
    end
  end
end

# ['cherry', 'seven', 'bell', 'stars', 'joker']
