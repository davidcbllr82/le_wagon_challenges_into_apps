require_relative 'black_jack'
require 'highline/import'

def state_of_the_game(player_score, bank_score)
  # TODO: return (not print!) a message containing the player's score and bank's score
  return "Your score is #{player_score}, bank is #{bank_score}"
end

def end_game_message(player_score, bank_score)
  # TODO: return (not print!) a message telling if the user won or lost.
  if player_score > 21
    "Sorry you lose, you passed 21"
  elsif player_score == 21
    "Black Jack!!"
  elsif player_score == bank_score
    "Push"
  elsif player_score > bank_score
    "You Win, your score is #{player_score} and the Bank's is #{bank_score}"
  else
    "Sorry you lose"
  end
  return
end


def play_game
  player.choose do |menu|
    menu.header = "Black Jack Game"
    menu.prompt = "Another card? "

    menu.choices(:y) { player.say("Card") }
    menu.choices(:n) { player.say("No more card") }
    until player.choose == menu.choices
      pick_bank_score += 1
      pick_player_card += 1
      state_of_the_game
      break if player.choose == menu.choices
    end
  end
end

def stop
  Game!
end
