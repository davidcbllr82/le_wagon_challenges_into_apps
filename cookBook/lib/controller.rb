require_relative 'recipe'
require_relative 'scraper'
require_relative 'view'
require 'pry-byebug'

class Controller
  def initialize(cookbook)
    @cookbook = cookbook
    @view = View.new
  end

  def list
    display_recipes
  end

  def create
    # 1. Ask user for a name (view)
    name = @view.ask_for("name")
    # 2. Ask user for a description (view)
    description = @view.ask_for("description")
    # 3. Ask user for preparation time
    prep_time = @view.ask_for("preparation time")
    # 3. Create recipe (model)
    recipe = Recipe.new(name: name, description: description, prep_time: prep_time)
    # 4. Store in cookbook (repo)
    @cookbook.add_recipe(recipe)
    # 5. Display
    display_recipes
  end

  def destroy
    display_recipes
    index = @view.ask_for_index
    @cookbook.remove_recipe(index)
    display_recipes
  end

  def import
    @keyword = @view.ask_for("Which ingredient?")
    @recipes_result = Scraper.new(@keyword).data_scraper
    @view.display(@recipes_result)
    index = @view.ask_for_index
    @cookbook.add_recipe(@recipes_result[index])
    display_recipes
  end

  def mark_as_done
    display_recipes
    index = @view.ask_for_index
    @cookbook.mark_recipe_as_done(index)
    display_recipes
  end

  private

  def display_recipes
    # 1. Get recipes (repo)
    recipes = @cookbook.all
    # 2. Display recipes in the terminal (view)
    @view.display(recipes)
  end
end




