require_relative 'recipe'
require 'nokogiri'
require 'open-uri'

class Scraper
  def initialize(keyword)
    @keyword = keyword
  end

  def data_scraper
    url = "https://www.marmiton.org/recettes/recherche.aspx?type=all&aqt=#{@keyword}"
    html_file = open(url).read
    doc = Nokogiri::HTML(html_file)
    recipes_result = []
    doc.search(".recipe-card").first(5).each do |element|
      name = element.search('.recipe-card__title').text.strip
      description = element.search('.recipe-card__description').text.strip
      prep_time = element.search('.recipe-card__duration__value').text.strip
      recipes_result << Recipe.new(name: name, description: description, prep_time: prep_time)
    end
    return recipes_result
  end
end
