class View
  def display(recipes)
    recipes.each_with_index do |recipe, index|
      state = recipe.done? ? '[X]' : '[ ]'
      puts "#{index + 1}. #{state} #{recipe.name}(#{recipe.prep_time}):\n #{recipe.description}"
    end
  end

  def ask_for(thing)
    puts "#{thing} ?"
    puts '> '
    gets.chomp
  end

  def ask_for_index
    puts "Which one?"
    puts '> '
    gets.chomp.to_i - 1
  end
end
