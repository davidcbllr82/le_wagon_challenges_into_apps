# App << Le Wagon Challenges
Completed challenges at Le Wagon and turning them into mini apps with Sinatra micro-framework:

- Black Jack (♣️)
- Task Manager (⚙️)
- Cook Book (📕)
- Slot Machine (🎰)
- Vending Machine (☕️)
- Age in Days (📅)
